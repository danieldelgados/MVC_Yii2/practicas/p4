<?php
/* @var $this yii\web\View */

use yii\widgets\DetailView;
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\grid\GridView;
use yii\bootstrap\Modal;

$this->title = 'Marcadores';
?>
<div class="site-index">

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "\n{items}",
        'columns' => [
            'id',
            ['attribute' => 'enlace',
                'label' => 'enlace',
                'format' => 'raw',
                'value' => function($data) {
                    return Html::a($data->enlace, $data->url, ['class' => 'profile-link']);
                },
            ],
            [
                'attribute' => 'descripcion',
                'label' => 'descripcion',
                'format' => 'raw',
                'value' => function($data) {
                    return Html::a($data->descripcion, ['site/index','id'=>$data->id], ['class' => 'profile-link']);
                }
            ],
        ],
    ]);
    ?>

    <?php
    Modal::begin([
       
        'header' => '<h2>Descripcion</h2>',        
              'footer'=>'<a href="#" class="btn btn-primary" data-dismiss="modal">Cerrar</a><a href='.$datos['url'].' class="btn btn-primary btn ">Visitar</a>',
        'closeButton' => ['label' => 'X'],
        //'toggleButton'=>['label'=>'Visitar'],
        'clientOptions'=>[
            "show"=>(($datos==NULL)?FALSE:TRUE),
        ]
        ]);

        echo $datos['larga'];

    Modal::end();
    ?>


</div>
