DROP DATABASE IF EXISTS practica4;
CREATE DATABASE practica4;
USE practica4;

CREATE TABLE marcadores(
  id int AUTO_INCREMENT,
  enlace varchar(255),
  descripcion varchar(255),
  larga text,
  privado bool,
  url varchar(255),
  PRIMARY KEY(id)   
  );