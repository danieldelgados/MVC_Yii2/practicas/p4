<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "marcadores".
 *
 * @property int $id
 * @property string $enlace
 * @property string $descripcion
 */
class Marcadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'marcadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['enlace', 'descripcion','url'], 'string', 'max' => 255],
            [['larga'],'string'],
            ['privado','default','value'=>false],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'enlace' => 'Enlace',
            'descripcion' => 'Descripcion',
            'larga'=>'Larga',            
            'url'=>'URL',
            'privado'=>'Privado',
        ];
    }
}
