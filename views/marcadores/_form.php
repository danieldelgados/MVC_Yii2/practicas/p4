<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Marcadores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="marcadores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'enlace')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'larga')->textInput(['minlength'=> true]) ?>
    
    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'privado')->dropDownList([
        0=>'No',
        1=>'Si',
        
        
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
        <?= Html::resetButton('Eliminar', ['class' => ' reset btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
